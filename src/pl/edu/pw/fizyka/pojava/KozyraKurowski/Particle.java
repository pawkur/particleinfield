package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 * 
 * Klasa opisująca cząstkę, zawiera listę lini do rysowania obrazu ruchu
 * 
 */
@SuppressWarnings("serial")
public class Particle extends Component {

	double Xposition;
	double Yposition;
	double Zposition;
	double oldXposition;
	double oldYposition;
	double oldZposition;
	boolean ifDipolField = false;

	ArrayList<Line> motionTrack = new ArrayList<Line>();
	int particleindex;

	public Particle(double x, double y, double z) {
		Xposition = x;
		Yposition = y;
		Zposition = z;
		oldXposition = Xposition;
		oldYposition = Yposition;

	}

	public void paint(Graphics g) {

		motionTrack.add(new Line((int) oldXposition, (int) oldYposition,
				(int) Xposition, (int) Yposition));

		if (Xposition >= 0 && Yposition >= 0)
			g.drawOval((int) Xposition, (int) Yposition, 3, 3);
		for (Line line : motionTrack) {
			g.drawLine(line.startPositionX, line.startPositionY,
					line.endPositionX, line.endPositionY);

		}
		if (ifDipolField)
			g.drawOval(300, 300, 10, 10);
		oldXposition = Xposition;
		oldYposition = Yposition;

	}

}
