package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * 
 * Panel do wyświetlania symulacji
 *
 */
@SuppressWarnings("serial")
public class SimulationPanel extends JPanel {
	volatile boolean ifSimulate;
	MainFrame reference;
	Particle particle;

	public SimulationPanel(MainFrame argument) {
		reference = argument;

		particle = new Particle(400.0, 200, 0);

		setIfSimulate(false);

	}

	public void paintComponent(Graphics g) {

		particle.paint(g);

	}

	public boolean isIfSimulate() {
		return ifSimulate;
	}

	public void setIfSimulate(boolean argument) {
		ifSimulate = argument;
	}

}
