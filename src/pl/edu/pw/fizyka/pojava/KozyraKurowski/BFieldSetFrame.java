package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

/**
 * 
 * Klasa ustawiająca zaawansowane parametry pola E i B
 *
 */

public class BFieldSetFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	MainFrame mainFrame;
	JSlider BAmplitudeSlider;
	JSlider BFrequencySlider;
	JSlider EXAmplitudeSlider;
	JSlider EXFrequencySlider;
	JSlider EYAmplitudeSlider;
	JSlider EYFrequencySlider;
	JSlider BConstSlider;
	JSlider EXConstSlider;
	JSlider EYConstSlider;
	JSlider BLinearSlider;
	JSlider EXLinearSlider;
	JSlider EYLinearSlider;
	JTextField BAmplitudeField;
	JTextField BFrequencyField;
	JTextField EXAmplitudeField;
	JTextField EXFrequencyField;
	JTextField EYAmplitudeField;
	JTextField EYFrequencyField;
	JTextField EYConstField;
	JTextField EXConstField;
	JTextField BConstField;
	JTextField EXLinearField;
	JTextField EYLinearField;
	JTextField BLinearField;
	JLabel BAmplitudeLabel;
	JLabel BFrequencyLabel;
	JLabel EXAmplitudeLabel;
	JLabel EXFrequencyLabel;
	JLabel EYAmplitudeLabel;
	JLabel EYFrequencyLabel;
	JLabel BConstLabel;
	JLabel EXConstLabel;
	JLabel EYConstLabel;
	JLabel BLinearLabel;
	JLabel EXLinearLabel;
	JLabel EYLinearLabel;

	@SuppressWarnings("static-access")
	public BFieldSetFrame(MainFrame M1) throws HeadlessException {
		mainFrame = M1;
		setSize(200, 640);
		setLayout(new GridLayout());

		JButton acceptButton = new JButton(
				mainFrame.chooser.labels.getString("Accept"));
		addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {

			}

			@Override
			public void windowClosed(WindowEvent e) {

			}

			@Override
			public void windowClosing(WindowEvent e) {

				mainFrame.simulationMenu.BFieldPaternMenu.setEnabled(true);
				mainFrame.simulationMenu.EFieldPaternMenu.setEnabled(true);
			}

			@Override
			public void windowDeactivated(WindowEvent e) {

			}

			@Override
			public void windowDeiconified(WindowEvent e) {

			}

			@Override
			public void windowIconified(WindowEvent e) {

			}

			@Override
			public void windowOpened(WindowEvent e) {

			}

		});
		// setTitle("Aby opuścić okno kliknij przycisk Akceptuj");
		setTitle(mainFrame.chooser.labels.getString("EndMesssage"));
		BAmplitudeSlider = new JSlider();
		BFrequencySlider = new JSlider();
		EXAmplitudeSlider = new JSlider();
		EXFrequencySlider = new JSlider();
		EYAmplitudeSlider = new JSlider();
		EYFrequencySlider = new JSlider();
		EYConstSlider = new JSlider();
		EXConstSlider = new JSlider();
		BConstSlider = new JSlider();
		EYLinearSlider = new JSlider();
		EXLinearSlider = new JSlider();
		BLinearSlider = new JSlider();
		EYConstField = new JTextField();
		EXConstField = new JTextField();
		BConstField = new JTextField();
		BAmplitudeField = new JTextField();
		BFrequencyField = new JTextField();
		EXAmplitudeField = new JTextField();
		EXFrequencyField = new JTextField();
		EYAmplitudeField = new JTextField();
		EYFrequencyField = new JTextField();
		EYLinearField = new JTextField();
		EXLinearField = new JTextField();
		BLinearField = new JTextField();
		BAmplitudeLabel = new JLabel(
				mainFrame.chooser.labels.getString("BAmplitudeLabel"));
		BFrequencyLabel = new JLabel(
				mainFrame.chooser.labels.getString("BFrequencyLabel"));
		EXAmplitudeLabel = new JLabel(
				mainFrame.chooser.labels.getString("EXAmplitudeLabel")); // zrobić
																			// napisy
		EXFrequencyLabel = new JLabel(
				mainFrame.chooser.labels.getString("EXFrequencyLabel"));
		EYAmplitudeLabel = new JLabel(
				mainFrame.chooser.labels.getString("EYAmplitudeLabel"));
		EYFrequencyLabel = new JLabel(
				mainFrame.chooser.labels.getString("EYFrequencyLabel"));
		BConstLabel = new JLabel(
				mainFrame.chooser.labels.getString("BConstLabel"));
		EXConstLabel = new JLabel(
				mainFrame.chooser.labels.getString("EXConstLabel"));
		EYConstLabel = new JLabel(
				mainFrame.chooser.labels.getString("EYConstLabel"));
		BLinearLabel = new JLabel(
				mainFrame.chooser.labels.getString("BLinearLabel"));
		EXLinearLabel = new JLabel(
				mainFrame.chooser.labels.getString("EXLinearLabel"));
		EYLinearLabel = new JLabel(
				mainFrame.chooser.labels.getString("EYLinearLabel"));

		BAmplitudeSlider.setMinimum(0);
		BAmplitudeSlider.setMaximum(50);
		BAmplitudeSlider.setValue(7);
		setSlider(BAmplitudeSlider, BAmplitudeField, 10);
		BFrequencySlider.setMinimum(0);
		BFrequencySlider.setMaximum(20);
		BFrequencySlider.setValue(10);
		setSlider(BFrequencySlider, BFrequencyField, 100);

		EXAmplitudeSlider.setMinimum(0);
		EXAmplitudeSlider.setMaximum(500);
		EXAmplitudeSlider.setValue(100);
		setSlider(EXAmplitudeSlider, EXAmplitudeField, 1);

		EXFrequencySlider.setMinimum(1);
		EXFrequencySlider.setMaximum(600);
		EXFrequencySlider.setValue(20);
		setSlider(EXFrequencySlider, EXFrequencyField, 1000);

		EYAmplitudeSlider.setMinimum(0);
		EYAmplitudeSlider.setMaximum(500);
		EYAmplitudeSlider.setValue(100);
		setSlider(EYAmplitudeSlider, EYAmplitudeField, 1);

		EYFrequencySlider.setMinimum(1);
		EYFrequencySlider.setMaximum(600);
		EYFrequencySlider.setValue(20);
		setSlider(EYFrequencySlider, EYFrequencyField, 1000);

		BConstSlider.setMinimum(10);
		BConstSlider.setMaximum(40);
		BConstSlider.setValue(20);
		setSlider(BConstSlider, BConstField, 10);

		EXConstSlider.setMinimum(20);
		EXConstSlider.setMaximum(300);
		EXConstSlider.setValue(30);
		setSlider(EXConstSlider, EXConstField, 1);

		EYConstSlider.setMinimum(20);
		EYConstSlider.setMaximum(300);
		EYConstSlider.setValue(30);
		setSlider(EYConstSlider, EYConstField, 1);

		BLinearSlider.setMinimum(2);
		BLinearSlider.setMaximum(200);
		BLinearSlider.setValue(7);
		setSlider(BLinearSlider, BLinearField, 1000);

		EXLinearSlider.setMinimum(1);
		EXLinearSlider.setMaximum(100);
		EXLinearSlider.setValue(100);
		setSlider(EXLinearSlider, EXLinearField, 10);

		EYLinearSlider.setMinimum(1);
		EYLinearSlider.setMaximum(100);
		EYLinearSlider.setValue(100);
		setSlider(EYLinearSlider, EYLinearField, 10);

		acceptButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.calculate.BAmplitude = (double) BAmplitudeSlider
						.getValue() / 10;
				mainFrame.calculate.BFrequency = (double) BFrequencySlider
						.getValue() / 100;

				mainFrame.calculate.EXAmplitude = (double) EXAmplitudeSlider
						.getValue();
				mainFrame.calculate.EXFrequency = (double) EXFrequencySlider
						.getValue() / 1000;
				mainFrame.calculate.EYAmplitude = (double) EYAmplitudeSlider
						.getValue();
				mainFrame.calculate.EYFrequency = (double) EYFrequencySlider
						.getValue() / 1000;
				mainFrame.calculate.BConstant = (double) BConstSlider
						.getValue() / 10;
				mainFrame.calculate.EXConstant = (double) EXConstSlider
						.getValue();
				mainFrame.calculate.EYConstant = (double) EYConstSlider
						.getValue();

				mainFrame.calculate.BLinear = (double) BLinearSlider.getValue() / 1000;
				mainFrame.calculate.EXLinear = (double) EXLinearSlider
						.getValue() / 10;
				mainFrame.calculate.EYLinear = (double) EYLinearSlider
						.getValue() / 10;

				mainFrame.simulationMenu.BFieldPaternMenu.setEnabled(true);
				mainFrame.simulationMenu.EFieldPaternMenu.setEnabled(true);
				setVisible(false);

			}
		});
		setLayout(new MigLayout());
		add(BAmplitudeLabel, "w 130! , split 2");
		add(BAmplitudeField, "wrap, w 40!");
		add(BAmplitudeSlider, "grow, wrap");
		add(BFrequencyLabel, "w 130!, split 2");
		add(BFrequencyField, " w 40!,wrap");
		add(BFrequencySlider, "grow, wrap");

		add(EXAmplitudeLabel, "w 130!, split 2");
		add(EXAmplitudeField, " w 40!,wrap");
		add(EXAmplitudeSlider, "grow, wrap");

		add(EXFrequencyLabel, "w 130!, split 2");
		add(EXFrequencyField, " w 40!,wrap");
		add(EXFrequencySlider, "grow, wrap");

		add(EYAmplitudeLabel, "w 130!, split 2");
		add(EYAmplitudeField, " w 40!,wrap");
		add(EYAmplitudeSlider, "grow, wrap");

		add(EYFrequencyLabel, "w 130!, split 2");
		add(EYFrequencyField, " w 40!,wrap");
		add(EYFrequencySlider, "grow, wrap");

		add(BConstLabel, "w 130!, split 2");
		add(BConstField, " w 40!,wrap");
		add(BConstSlider, "grow, wrap");

		add(EXConstLabel, "w 130!, split 2");
		add(EXConstField, " w 40!,wrap");
		add(EXConstSlider, "grow, wrap");

		add(EYConstLabel, "w 130!, split 2");
		add(EYConstField, " w 40!,wrap");
		add(EYConstSlider, "grow, wrap");

		add(BLinearLabel, "w 130!, split 2");
		add(BLinearField, " w 40!,wrap");
		add(BLinearSlider, "grow, wrap");

		add(EXLinearLabel, "w 130!, split 2");
		add(EXLinearField, " w 40!,wrap");
		add(EXLinearSlider, "grow, wrap");

		add(EYLinearLabel, "w 130!, split 2");
		add(EYLinearField, " w 40!,wrap");
		add(EYLinearSlider, "grow, wrap");

		add(acceptButton, "gaptop 20, grow");

	}

	public void setSlider(final JSlider slider, final JTextField field,
			final double proportion) {// setslider nie działa tak jakbyś chciał

		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				field.setText(Double.toString((double) slider.getValue()
						/ proportion));
			}

		});
		field.setEnabled(false);
		slider.setEnabled(false);
	}

	public BFieldSetFrame(GraphicsConfiguration arg0) {
		super(arg0);

	}

	public BFieldSetFrame(String arg0) throws HeadlessException {
		super(arg0);

	}

	public BFieldSetFrame(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);

	}

	public void SetAllESlidersToFalse() {
		EXAmplitudeSlider.setEnabled(false);
		EYAmplitudeSlider.setEnabled(false);
		EXFrequencySlider.setEnabled(false);
		EYFrequencySlider.setEnabled(false);
		EXConstSlider.setEnabled(false);
		EYConstSlider.setEnabled(false);
		EXLinearField.setEnabled(false);
		EYLinearField.setEnabled(false);
	}

	public void SetAllBSlidersToFalse() {
		BAmplitudeSlider.setEnabled(false);
		BFrequencySlider.setEnabled(false);
		BConstSlider.setEnabled(false);
		BLinearSlider.setEnabled(false);
	}

}
