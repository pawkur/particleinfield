package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

/**
 * @author Paweł Kurowski
 * Klasa obługująca wybieranie języka
 *
 */

@SuppressWarnings("serial")
public class LanguageChooser extends JFrame {

	Locale locale;
	ResourceBundle labels;
	JComboBox<String> languages;
	MainFrame interfaceFrame;

	public LanguageChooser() {
		setVisible(true);
		setSize(250, 100);
		setLayout(new MigLayout());
		languages = new JComboBox<String>();
		JLabel languagelabel = new JLabel("Language:");
		JButton start = new JButton("Start");
		JPanel panelWithEverything = new JPanel();
		add(panelWithEverything);
		panelWithEverything.add(languagelabel);
		panelWithEverything.add(languages);
		panelWithEverything.add(start);
		languages.addItem("Polski");
		languages.addItem("English");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String stringWithLanguage = languages.getSelectedItem()
						.toString();
				if (stringWithLanguage.equals("Polski")) {
					locale = new Locale("pl", "PL", "UNIX");
				} else if (stringWithLanguage.equals("English")) {
					locale = new Locale("en", "GB", "UNIX");

				}
				labels = ResourceBundle.getBundle("LBundle", locale);

				MainFrame mainFrame = new MainFrame();
				BFieldSetFrame b = new BFieldSetFrame(mainFrame);
				mainFrame.simulationMenu.addFrame(b);
				b.setVisible(false);
				mainFrame.setVisible(true);
				mainFrame.simulationThread.start();
				dispose();
			}
		});
	}
}