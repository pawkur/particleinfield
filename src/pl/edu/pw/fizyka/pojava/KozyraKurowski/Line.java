package pl.edu.pw.fizyka.pojava.KozyraKurowski;

/**
 * 
 * Klasa opisujaca linie, które staną się obrazem ruchu cząstki.
 *
 */

public class Line {
	int startPositionX;
	int startPositionY;
	int endPositionX;
	int endPositionY;

	public Line(int sx, int sy, int ex, int ey) {
		startPositionX = sx;
		startPositionY = sy;
		endPositionX = ex;
		endPositionY = ey;
	}

}
