package pl.edu.pw.fizyka.pojava.KozyraKurowski;

/**
 * 
 * Opis działania wątku pobocznego.
 * 
 */

public class SimulationRunnable implements Runnable {

	MainFrame reference;

	public SimulationRunnable(MainFrame argument) {
		reference = argument;

	}

	@Override
	public void run() {

		while (true) {

			while (reference.simulationPanel.isIfSimulate()) {

				for (int i = 0; i < 50000; i++)
					reference.calculate.setLocation(
							reference.simulationPanel.particle,
							reference.simulationMenu.ParticleIndex);

				try {
					reference.repaint();
					Thread.sleep(reference.simulationMenu.speedOfSimulation);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}

			}
		}

	}

}
