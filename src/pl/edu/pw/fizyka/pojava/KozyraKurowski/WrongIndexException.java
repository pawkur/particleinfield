package pl.edu.pw.fizyka.pojava.KozyraKurowski;

/**
 * @author Łukasz Kozyra
 * Klasa opisująca wyjętek przy złym indeksie
 * 
 */

public class WrongIndexException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public WrongIndexException() {

	}

	public WrongIndexException(String arg0) {
		super(arg0);

	}

	public WrongIndexException(Throwable arg0) {
		super(arg0);

	}

	public WrongIndexException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	public WrongIndexException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

}
