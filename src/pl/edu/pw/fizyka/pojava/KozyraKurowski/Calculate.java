package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import javax.swing.JOptionPane;

/**
 * 
 * Klasa obługująca obliczenia fizyczne
 *
 */

public class Calculate {
	SimulationMenu simulationMenu;
	final static double electronMass = 9.10938291 * Math.pow(10, -31);

	final static double protonMass = 1.672621638 * Math.pow(10, -27);
	final static double elementaryCharge = 1.602176565 * Math.pow(10, -19);
	static double scale = Math.pow(10, 2);
	double BDipolConst = 100;
	double EDipolConst = Math.pow(9, 9);
	MainFrame reference;
	final static int CONSTANT_FIELD = 1;
	final static int SINUS = 2;
	final static int LINEAR = 3;
	final static int NO_FIELD = 4;
	final static int DIPOLFIELD = 5;
	final static int POSITON = 1;

	final static int ELECTRON = 2;
	final static int DIPOL = 3;
	double x;
	double y;
	double xVelocity = 0;
	double yVelocity = 4;
	double zVelocity = 0;
	double xAcceleration;
	double yAcceleration;
	double zAcceleration;
	double time = 0.00001;
	double ErunningTime = 0.1;
	double BrunningTime = 0.1;

	double mass;
	double charge;
	double xEAcceleration;
	double yEAcceleration;
	double xBAcceleration;
	double yBAcceleration;
	double zBAcceleration;
	double zEAcceleration;

	double Ey;
	double Ez;
	double Ex;
	double Bx;
	double Bz;
	double By;
	double EYConstant = 30;// 20-300
	double EXConstant = 20;// 20-300
	double BConstant = 2;// 1-4
	double EXAmplitude = 100;// 0-500
	double EYAmplitude = 100;// 0-500
	double BAmplitude = 2;
	double EXFrequency = 0.02;// 0.001-0.6
	double EYFrequency = 0.02;// 0.001-0.6
	double BFrequency = 0.01;
	double EXLinear = 10;// 0-10
	double EYLinear = 10;// 0-10
	double BLinear = 0.006;// 0.002-0.2
	double DipolMomentum = Math.pow(10, -4);
	double EDipolMomentum = Math.pow(10, -13);

	double XDipolPosition = 300;
	double YDipolPosition = 300;
	double ZDipolPosition = 0;

	boolean if10xMass = false;

	public Calculate(MainFrame argument) {
		reference = argument;
		simulationMenu = reference.simulationMenu;

	}

	public void setParticle(int particleIndex) {
		switch (particleIndex) {
		case POSITON:
			if (if10xMass) {
				mass = 10 * electronMass;
			} else {
				mass = electronMass;

			}
			charge = elementaryCharge;
			break;
		case ELECTRON:
			if (if10xMass) {
				mass = 10 * electronMass;
			} else {
				mass = electronMass;
			}
			charge = (-1) * elementaryCharge;
			break;
		default:
			throw new WrongIndexException();
		}
	}

	public void setField(int indexE, int indexB, Particle particle) {
		if (indexE == DIPOLFIELD || indexB == DIPOLFIELD) {
			particle.ifDipolField = true;
		} else {
			particle.ifDipolField = false;
		}
		switch (indexE) {
		case CONSTANT_FIELD:
			Ex = EXConstant * Math.pow(25, -10);
			Ey = EYConstant * Math.pow(25, -10);
			break;
		case SINUS:
			Ex = EXAmplitude * Math.pow(25, -10)
					* Math.sin(EXFrequency * Math.pow(1, -10) * ErunningTime);
			Ey = EYAmplitude * Math.pow(25, -10)
					* Math.sin(EYFrequency * Math.pow(1, -10) * ErunningTime);

			break;
		case LINEAR:

			Ex = EXLinear * Math.pow(25, -10) * ErunningTime;
			Ey = EYLinear * Math.pow(25, -10) * ErunningTime;
			if (ErunningTime > 100)
				ErunningTime = -100;
			break;
		case NO_FIELD:
			Ex = 0;
			Ey = 0;
			break;
		case DIPOLFIELD:
			double R = Math.sqrt((particle.Xposition - XDipolPosition)
					* (particle.Xposition - XDipolPosition)
					+ (particle.Yposition - YDipolPosition)
					* (particle.Yposition - YDipolPosition)
					+ (particle.Zposition - ZDipolPosition)
					* (particle.Zposition - ZDipolPosition));
			Ex = EDipolConst * EDipolMomentum / Math.pow(R, 5) * 3
					* (particle.Xposition - XDipolPosition)
					* (particle.Yposition - YDipolPosition);
			Ey = EDipolConst
					* EDipolMomentum
					/ Math.pow(R, 3)
					* (3 * (particle.Yposition - YDipolPosition)
							* (particle.Yposition - YDipolPosition)
							/ Math.pow(R, 2) - 1);
			Ez = EDipolConst * EDipolMomentum / Math.pow(R, 5) * 3
					* (particle.Zposition - ZDipolPosition)
					* (particle.Yposition - YDipolPosition);
			break;
		default:
			throw new WrongIndexException();

		}
		ErunningTime = ErunningTime + time;

		switch (indexB) {
		case 0:
			JOptionPane.showMessageDialog(null, "Brak pola B");
			break;
		case CONSTANT_FIELD:
			Bz = BConstant * Math.pow(250, -5);
			break;
		case SINUS:
			Bz = BAmplitude * Math.pow(250, -5)
					* Math.sin(BFrequency * Math.pow(1, -10) * BrunningTime);
			break;
		case LINEAR:

			Bz = BLinear * Math.pow(250, -5) * BrunningTime;
			if (BrunningTime > 60)
				BrunningTime = 0;
			break;
		case NO_FIELD:
			Bz = 0;

			break;
		case DIPOLFIELD:

			double R = Math.sqrt((particle.Xposition - XDipolPosition)
					* (particle.Xposition - XDipolPosition)
					+ (particle.Yposition - YDipolPosition)
					* (particle.Yposition - YDipolPosition)
					+ (particle.Zposition - ZDipolPosition)
					* (ZDipolPosition - particle.Zposition));
			Bz = DipolMomentum * BDipolConst * Math.pow(10, -7)
					/ Math.pow(R, 4) * 3
					* (particle.Yposition - YDipolPosition)
					* (particle.Xposition - ZDipolPosition);
			By = DipolMomentum
					* BDipolConst
					* Math.pow(10, -7)
					/ Math.pow(R, 4)
					* (3 * (particle.Yposition - YDipolPosition)
							* (particle.Yposition - YDipolPosition) - DipolMomentum
							* R);
			Bx = DipolMomentum * BDipolConst * Math.pow(10, -7)
					/ Math.pow(R, 4) * 3
					* (particle.Yposition - YDipolPosition)
					* (particle.Zposition - ZDipolPosition);

			break;
		default:
			throw new WrongIndexException();

		}
		BrunningTime = BrunningTime + time;

	}

	public void setVelocity() {
		xBAcceleration = charge / mass * (yVelocity * Bz - zVelocity * By);
		yBAcceleration = charge / mass * (zVelocity * Bx - xVelocity * Bz);
		zBAcceleration = charge / mass * (xVelocity * By - yVelocity * Bx);
		xEAcceleration = charge / mass * Ex;
		yEAcceleration = charge / mass * Ey;
		zEAcceleration = charge / mass * Ez;
		zAcceleration = zEAcceleration + zBAcceleration;
		xAcceleration = xEAcceleration + xBAcceleration;
		yAcceleration = yEAcceleration + yBAcceleration;
		xVelocity += xAcceleration * time;
		yVelocity += yAcceleration * time;
		zVelocity += zAcceleration * time;
	}

	public void setLocation(Particle p, int particleIndex) {

		setField(simulationMenu.EFieldindex, simulationMenu.BFieldindex, p);
		setVelocity();

		p.Zposition = (zVelocity * time + p.Zposition);
		p.Xposition = (xVelocity * time + p.Xposition);
		p.Yposition = (yVelocity * time + p.Yposition);

	}

	public double getX() {
		return x;
	}

	public void setX(double argument) {
		x = argument;
	}

	public void clean() {
		Bx = 0;
		By = 0;
		Bz = 0;
		Ex = 0;
		Ey = 0;
		Ez = 0;
		xEAcceleration = 0;
		yEAcceleration = 0;
		zEAcceleration = 0;
		xBAcceleration = 0;
		yBAcceleration = 0;
		zBAcceleration = 0;
		xVelocity = 0;
		yVelocity = 4;
		zVelocity = 0;
		BrunningTime = 0.1;
		ErunningTime = 0.1;

	}
}
