package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import java.awt.Dimension;
import java.awt.HeadlessException;

import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;

/**
 * 
 * Klasa główna, ramka programu.
 *
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	SimulationPanel simulationPanel;
	SimulationMenu simulationMenu;
	Calculate calculate;
	Thread simulationThread;
	static LanguageChooser chooser;

	public MainFrame() throws HeadlessException {

		simulationPanel = new SimulationPanel(this);
		simulationMenu = new SimulationMenu(this);
		calculate = new Calculate(this);
		simulationThread = new Thread(new SimulationRunnable(this));

		setLayout(new MigLayout());
		setSize(780, 480);
		setMinimumSize(new Dimension(780, 480));
		setTitle(simulationMenu.frameTitle);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		add(simulationMenu, "dock west, growy");
		add(simulationPanel, "dock center, grow");

	}

	public static void main(String[] args) {

		chooser = new LanguageChooser();

	}
}
