package pl.edu.pw.fizyka.pojava.KozyraKurowski;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

/**
 * 
 * Panel z podstawowymi parametrami programu, przyciskami.
 *
 */
@SuppressWarnings("serial")
public class SimulationMenu extends JPanel {

	MainFrame reference;
	int EFieldindex = 0;
	int BFieldindex = 0;
	int ParticleIndex = 0;
	volatile int speedOfSimulation = 20;
	JComboBox<String> EFieldPaternMenu;
	JComboBox<String> BFieldPaternMenu;
	JButton optionFieldButton;
	final static int CONSTANT_FIELD = 1;
	final static int SINUS = 2;
	final static int LINEAR = 3;
	final static int NO_FIELD = 4;
	final static int DIPOL_FIELD = 5;

	BFieldSetFrame optionFrame;

	JCheckBox ChoseProton;
	JCheckBox ChoseElectron;
	JCheckBox ChoseDipol;
	JSlider speedSlider;
	JLabel speedLabel;
	JTextField speedField;
	JLabel speedFieldLabel;
	JButton ChoseEnglish;
	JButton ChosePolish;
	JButton simulationTrigger;
	String frameTitle;
	BFieldSetFrame optionFieldReference;
	JCheckBox multipleMass;
	Boolean multipleMassLogic = false;

	@SuppressWarnings("static-access")
	public SimulationMenu(MainFrame argument) {
		reference = argument;
		setLayout(new MigLayout());
		setBorder(BorderFactory.createEtchedBorder());

		// LISTA E I B
		EFieldPaternMenu = new JComboBox<String>();// typ E
		BFieldPaternMenu = new JComboBox<String>(); // typ B

		EFieldPaternMenu.addItem(reference.chooser.labels.getString("const"));
		EFieldPaternMenu.addItem(reference.chooser.labels.getString("sin"));
		EFieldPaternMenu.addItem(reference.chooser.labels.getString("linear"));
		EFieldPaternMenu.addItem(reference.chooser.labels.getString("nofield"));
		EFieldPaternMenu.addItem(reference.chooser.labels.getString("dipol"));

		BFieldPaternMenu.addItem(reference.chooser.labels.getString("const"));
		BFieldPaternMenu.addItem(reference.chooser.labels.getString("sin"));
		BFieldPaternMenu.addItem(reference.chooser.labels.getString("linear"));
		BFieldPaternMenu.addItem(reference.chooser.labels.getString("nofield"));
		BFieldPaternMenu.addItem(reference.chooser.labels.getString("dipol"));

		ChoseProton = new JCheckBox(
				reference.chooser.labels.getString("Proton"));
		ChoseElectron = new JCheckBox(
				reference.chooser.labels.getString("Electron"));

		final JCheckBox multipleMass = new JCheckBox(
				reference.chooser.labels.getString("multipleMass"));

		speedSlider = new JSlider();

		frameTitle = reference.chooser.labels.getString("particleInField");
		speedLabel = new JLabel(reference.chooser.labels.getString("slow1"));
		speedFieldLabel = new JLabel(
				reference.chooser.labels.getString("slow2"));

		speedField = new JTextField("20");
		simulationTrigger = new JButton(
				reference.chooser.labels.getString("Start"));

		// DODAWANIE ACTION LISTENEROW

		multipleMass.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (multipleMassLogic) {
					reference.calculate.if10xMass = false;
					multipleMassLogic = false;
				} else {
					reference.calculate.if10xMass = true;
					multipleMassLogic = true;
				}
			}
		});

		ChoseProton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ChoseElectron.setSelected(false);
				ChoseProton.setSelected(true);
				ParticleIndex = 1;

			}
		});
		ChoseElectron.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				ChoseProton.setSelected(false);
				ChoseElectron.setSelected(true);
				ParticleIndex = 2;
			}
		});

		ChoseElectron.doClick();

		speedField.setEnabled(false);

		speedSlider.setMinimum(0); // Spowolnienie symulacji
		speedSlider.setMaximum(100);
		speedSlider.setValue(20);
		speedSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				speedOfSimulation = speedSlider.getValue();
				speedField.setText(Double.toString(speedOfSimulation));
			}
		});

		ActionListener SimulationTriggerLisiner = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (reference.simulationPanel.ifSimulate == true) {

					reference.calculate.clean();

					reference.simulationPanel.ifSimulate = false;
					simulationTrigger.setText(reference.chooser.labels
							.getString("Start"));

				} else {
					reference.calculate.clean();
					reference.simulationPanel.particle.Xposition = 180;
					reference.simulationPanel.particle.Yposition = 301;
					reference.simulationPanel.particle.Zposition = 0;
					reference.simulationPanel.particle.oldXposition = 180;
					reference.simulationPanel.particle.oldYposition = 301;

					reference.simulationPanel.particle.motionTrack.clear();
					setEFieldType(EFieldPaternMenu.getSelectedIndex());
					setBFieldType(BFieldPaternMenu.getSelectedIndex());

					reference.calculate.setParticle(ParticleIndex);
					reference.simulationPanel.paint(reference.simulationPanel
							.getGraphics());
					reference.simulationPanel.ifSimulate = true;
					simulationTrigger.setText("Stop");

				}

			}

		};
		simulationTrigger.addActionListener(SimulationTriggerLisiner);
		optionFieldButton = new JButton(
				reference.chooser.labels.getString("Options"));
		ActionListener OptionWindowTrigger = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				optionFrame.EYConstField.setText(Double
						.toString((double) reference.calculate.EYConstant));
				optionFrame.EXConstField.setText(Double
						.toString((double) reference.calculate.EXConstant));
				optionFrame.BConstField.setText(Double
						.toString((double) reference.calculate.BConstant));
				optionFrame.BAmplitudeField.setText(Double
						.toString((double) reference.calculate.BAmplitude));
				optionFrame.BFrequencyField.setText(Double
						.toString((double) reference.calculate.BFrequency));
				optionFrame.EXAmplitudeField.setText(Double
						.toString((double) reference.calculate.EXAmplitude));
				optionFrame.EXFrequencyField.setText(Double
						.toString((double) reference.calculate.EYFrequency));
				optionFrame.EYAmplitudeField.setText(Double
						.toString((double) reference.calculate.EYAmplitude));
				optionFrame.EYFrequencyField.setText(Double
						.toString((double) reference.calculate.EYFrequency));
				optionFrame.EYLinearField.setText(Double
						.toString((double) reference.calculate.EYLinear));
				optionFrame.EXLinearField.setText(Double
						.toString((double) reference.calculate.EXLinear));
				optionFrame.BLinearField.setText(Double
						.toString((double) reference.calculate.BLinear));
				optionFrame.setVisible(true);
				EFieldPaternMenu.setEnabled(false);
				BFieldPaternMenu.setEnabled(false);
				int Efield = EFieldPaternMenu.getSelectedIndex() + 1;
				switch (Efield) {
				case CONSTANT_FIELD:
					optionFrame.SetAllESlidersToFalse();
					optionFrame.EXConstSlider.setEnabled(true);
					optionFrame.EYConstSlider.setEnabled(true);

					break;
				case SINUS:
					optionFrame.SetAllESlidersToFalse();
					optionFrame.EXAmplitudeSlider.setEnabled(true);
					optionFrame.EYAmplitudeSlider.setEnabled(true);
					optionFrame.EXFrequencySlider.setEnabled(true);
					optionFrame.EYFrequencySlider.setEnabled(true);

					break;
				case LINEAR:
					optionFrame.SetAllESlidersToFalse();
					optionFrame.EXLinearSlider.setEnabled(true);
					optionFrame.EYLinearSlider.setEnabled(true);

					break;
				case NO_FIELD:
					optionFrame.SetAllESlidersToFalse();
					break;
				case DIPOL_FIELD:
					optionFrame.SetAllESlidersToFalse();
					break;
				default:
					throw new WrongIndexException();

				}
				int Bfield = BFieldPaternMenu.getSelectedIndex() + 1;
				switch (Bfield) {
				case CONSTANT_FIELD:
					optionFrame.SetAllBSlidersToFalse();
					optionFrame.BConstSlider.setEnabled(true);

					break;
				case SINUS:
					optionFrame.SetAllBSlidersToFalse();
					optionFrame.BAmplitudeSlider.setEnabled(true);
					optionFrame.BFrequencySlider.setEnabled(true);

					break;
				case LINEAR:
					optionFrame.SetAllBSlidersToFalse();
					optionFrame.BLinearSlider.setEnabled(true);
					break;
				case NO_FIELD:
					optionFrame.SetAllBSlidersToFalse();
					break;
				case DIPOL_FIELD:
					optionFrame.SetAllBSlidersToFalse();
				default:
					throw new WrongIndexException();

				}

			}

		};
		optionFieldButton.addActionListener(OptionWindowTrigger);
		add(new JLabel(reference.chooser.labels.getString("fieldE")),
				"grow, split 2");
		add(new JLabel(reference.chooser.labels.getString("fieldB")),
				"grow, wrap");
		add(EFieldPaternMenu, "grow, split 2");
		add(BFieldPaternMenu, "grow, wrap");
		add(ChoseProton, "grow, split 3");
		add(ChoseElectron, "grow, wrap");
		add(speedLabel, "wrap");
		add(speedSlider, "grow, wrap");
		add(speedFieldLabel, "split 2, grow");
		add(speedField, "w 50!, wrap");
		add(simulationTrigger, "wrap ,grow");
		add(optionFieldButton, "grow, wrap");
		add(multipleMass);
	}

	public void addFrame(BFieldSetFrame b) {
		optionFrame = b;
	}

	void setEFieldType(int type) {
		if (type <= 4 || type >= 0) {
			EFieldindex = type + 1;
		} else {
			throw new WrongIndexException();
		}

	}

	void setBFieldType(int type) {
		if (type <= 4 || type >= 0) {
			BFieldindex = type + 1;
		} else {
			throw new WrongIndexException();
		}

	}
}
